# archninja

This bash program will allow Arch veterans (and those new to Arch if they have
familiarity with Linux) to quickly install and configure Arch Linux.

My goal in writing this program is to automate the manual tasks of installing
Arch Linux as much as I feel it makes sense.

Some of the decisions of how to install Arch Linux are made by me based on my
experience. You may not agree with my decisions. It is therefore upon you to
make the changes you see fit so that the outcome of this program is to your
liking.

## Usage:

Download the Arch Linux Installation ISO
[latest Arch Linux ISO](https://www.archlinux.org/download/)

You should create a bootable disc from the ISO, either USB or a CDROM, and boot
your PC using the newly created disc as the boot device.

Once you have the Arch ISO booted, select the first menu option which will take
you to a root shell prompt.

Download the archninja program from GitLab and make it executable:

[archninja](https://thelinuxninja.gitlab.io/archninja)

~~~sh
wget -O archninja https://thelinuxninja.gitlab.io/archninja
chmod u+x archninja
./archninja
~~~

# Influencers

I have been a longtime Debian user and have always loved the text-based Debian
installer. However, Debian has it's place in the Linux community, but it's not
what I prefer as a daily desktop. I also have come to find that I need to be
able to standup virtual machines for testing, and I don't need to spend a lot of
time on their setup.

I did find a project that did a lot of what I was looking for, but it was very
rough around the edges. The author wasn't on-board with the changes I was hoping
could be made to improve the project, and I felt I could do better if I just
started my own project.

That's what has led me to begin writing this program. I hope I get to finish it.
